import axios, { CreateAxiosDefaults, AxiosRequestHeaders } from 'axios'

const TOKEN_CYBERSOFT = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA1OCIsIkhldEhhblN0cmluZyI6IjExLzA2LzIwMjQiLCJIZXRIYW5UaW1lIjoiMTcxODA2NDAwMDAwMCIsIm5iZiI6MTY5MDM5MDgwMCwiZXhwIjoxNzE4MjExNjAwfQ.631rl3EwTQfz6CuufNTJlys36XLVmoxo29kP-F_PDKU';

export const apiInstance = (config?: CreateAxiosDefaults) => { 
    const accessTokenJson = JSON.parse(localStorage.getItem('accessToken'));
    if(accessTokenJson){
        var accessToken = accessTokenJson;
    }
    const api = axios.create(config);
    api.interceptors.request.use((config) => { 
        return {
            ... config,
            headers: {
                Authorization: 'Bearer' + ' ' + accessToken,
                TokenCybersoft: TOKEN_CYBERSOFT
            } as unknown as AxiosRequestHeaders
        }
    })
    return api;
}