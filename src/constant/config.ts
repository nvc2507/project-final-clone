export const PATH = {
    login: '/login',
    register: '/register',

    detail: '/detail/:maKhoaHoc',
    account: '/account',

    detailCourses: '/khoaHocTheoDanhMuc/:maDanhMuc',
    profile: "/profile",
    searchPage: '/search/:tenKhoaHoc',

    // Admin page
    admin: '/admin',
    adminCourse: '/admin-course',
    adminAddCourse: '/admin-addcourse',
    adminUpdateCourse: "/admin-updatecourse/:id",
    adminDetailCourse: "/admin-detailcourse/:id",

    adminUser: '/admin-users',
    adminUpdateUser: "/admin-updateuser/:id",
    adminAddUser: "/admin-adduser",
    adminDetailUser: "/admin-detailuser/:id",
}