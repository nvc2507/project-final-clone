import { AuthLayout, MainLayout } from "components/layouts";
import ProfileTemplate from "components/template/Profile/ProfileTemplate";
import { PATH } from "constant";
import { Home, Login, Register } from "pages";
import Account from "pages/Account";
import AddCourse from "pages/AdminPage/AddCourse";
import AdminCoursePage from "pages/AdminPage/AdminCoursePage";
import AdminProfile from "pages/AdminPage/AdminProfile";
import DetailCourse from "pages/AdminPage/DetailCourse/DetailCourse";
import UpdateCourse from "pages/AdminPage/UpdateCourse";

import AddUser from "pages/AdminUserPage/AddUser";
import AdminUsersPage from "pages/AdminUserPage/AdminUserPage";
import DetailUser from "pages/AdminUserPage/DetailUser/DetailUser";
import UpdateUser from "pages/AdminUserPage/UpdateUser";
import Detail from "pages/Detail";
import DetailCourses from "pages/DetailCourses";
import Search from "pages/Search";

import { RouteObject } from "react-router-dom";

export const router: RouteObject[] = [
    {
        element: <MainLayout></MainLayout>,
        children: [
            {
                path: '/',
                element: <Home></Home>
            },
            {
                path: PATH.detail,
                element: <Detail></Detail>
            },
            {
                path: PATH.account,
                element: <Account></Account>
            },
            {
                path: PATH.detailCourses,
                element: <DetailCourses />
            },
            {
                path: PATH.profile,
                element: <ProfileTemplate/>
            },
            {
                path: PATH.searchPage,
                element: <Search/>
            },
           
           


        ]
    },
     //Admin page
     {  
        element: <AdminProfile />,
        children: [
            {
                path: PATH.adminCourse,
                element: <AdminCoursePage />, 
            },
            {
                path: PATH.adminUpdateCourse,
                element: <UpdateCourse />, 
            },
            {
                path: PATH.adminDetailCourse,
                element: <DetailCourse />, 
            },
            {
                path: PATH.adminAddCourse,
                element: <AddCourse />, 
            },

            {
                path: PATH.adminUser,
                element: <AdminUsersPage />, 
            },
            {
                path: PATH.adminDetailUser,
                element: <DetailUser />, 
            },
            {
                path: PATH.adminUpdateUser,
                element: <UpdateUser />, 
            },
            {
                path: PATH.adminAddUser,
                element: <AddUser />, 
            },
        ]
    },
    {
        element: <AuthLayout></AuthLayout>,
        children: [
            {
                path: PATH.login,
                element: <Login></Login>
            },
            {
                path: PATH.register,
                element: <Register></Register>
            }
        ]
    }
] 