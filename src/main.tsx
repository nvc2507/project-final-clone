import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'
import { BrowserRouter } from 'react-router-dom'
import { StyleProvider } from '@ant-design/cssinjs';
import './assets/style.css'
import { Provider } from 'react-redux'
import { store } from 'stores';
import { ToastContainer } from 'react-toastify';
  import 'react-toastify/dist/ReactToastify.css';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <BrowserRouter>
    <Provider store={store}>
      <StyleProvider hashPriority="high">
        <ToastContainer></ToastContainer>
        <App />
      </StyleProvider>
    </Provider>
  </BrowserRouter>
)
