import { message } from "antd";
import * as validator from "validator";

export const checkUserName = (username: string): boolean => {
    const isValidUsername = validator.matches(username, /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/);
    if (isValidUsername) {
        return true;
    } else {
        message.error('Tài khoản ít nhất 6 ký tự bao gồm cả chữ và số!');
        return false;
    }
};

export const checkPassword = (password: string): boolean => {
    const isValidPassword = validator.isLength(password.trim(), { min: 4 });
    if (isValidPassword) {
        return true;
    } else {
        message.error('Mật khẩu ít nhất 4 ký tự trở lên!');
        return false;
    }
};

export const repeatPassword = (password: string, repeatPassword: string): boolean => {
    const isValidPassword = validator.equals(password, repeatPassword);
    if (isValidPassword) {
        return true;
    } else {
        message.error("Mật khẩu không khớp!");
        return false;
    }
};

export const checkFullName = (fullName: string): boolean => {
    const isFullNameValid = validator.matches(fullName, /[^a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễếệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]/u);
    if (isFullNameValid) {
        return true;
    } else {
        message.error("Họ tên không hợp lệ!");
        return false;
    }
};

export const checkEmail = (email: string): boolean => {
    const isValidEmail = validator.isEmail(email);
    if (isValidEmail) {
        return true;
    } else {
        message.error("Email không hợp lệ");
        return false;
    }
};

export const checkPhoneVietNam = (soDt: string): boolean => {
    const isValidPhone = validator.isMobilePhone(soDt, 'vi-VN');
    if (isValidPhone) {
        return true;
    } else {
        message.error('Số điện thoại không hợp lệ!');
        return false;
    }
};
