import SearchPageTemplate from "components/template/SearchPage/SearchPageTemplate"

const Search = () => {
  return (
    <div>
        <SearchPageTemplate />
    </div>
  )
}

export default Search