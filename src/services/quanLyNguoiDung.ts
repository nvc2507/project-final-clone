import { apiInstance } from "constant"

import { AccountSchemaType, LoginSchemaType, RegisterSchemaType } from "schema"
import { DanhSachNguoiDung_PhanTrang, QueryLayDanhSachNguoiDung_PhanTrang, QueryTimKiemNguoiDung, User } from "types"
import { generateSearchString } from "utils"

const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_NGUOI_DUNG_API
})

export const quanLyNguoiDungServices = {
    login: (payload: LoginSchemaType) => api.post<User>('/DangNhap', payload),
    register: (payload: RegisterSchemaType) => api.post('/DangKy', payload),
    layDanhSachNguoiDung: () => api.get<User[]>('/LayDanhSachNguoiDung'),
    xoaNguoiDung: (payload: string) => api.delete(`/XoaNguoiDung?TaiKhoan=${payload}`),
    layDanhSachNguoiDung_PhanTrang: (payload: QueryLayDanhSachNguoiDung_PhanTrang) => {
        const path = '/LayDanhSachNguoiDung_PhanTrang';
        const seachString = generateSearchString({
            MaNhom: payload.MaNhom,
            tuKhoa: payload.tuKhoa,
            page: payload.page,
            pageSize: payload.pageSize
        })
        return api.get<DanhSachNguoiDung_PhanTrang>(path + seachString)
    },
    themNguoiDung: (payload: AccountSchemaType) => api.post('/ThemNguoiDung', payload),
    capNhatThongTinNguoiDung: (payload: AccountSchemaType) => api.put('/CapNhatThongTinNguoiDung', payload),
    timKiemNguoiDung: (payload: QueryTimKiemNguoiDung) => {
        const path = '/TimKiemNguoiDung';
        const seachString = generateSearchString({
            MaNhom: payload.MaNhom,
            tuKhoa: payload.tuKhoa
        });
        return api.get<User[]>(path + seachString);
    },
    layThongTinNguoiDung: (payload: any) => api.post<User>(`/ThongTinNguoiDung?taiKhoan=${payload} `),
    getListUser: (payload: any) => api.get<User>(`/LayDanhSachNguoiDung?MaNhom=${payload}`),
    getListUserInfno: (payload: any) => api.get<User>(`/LayDanhSachNguoiDung?taiKhoan=${payload}`),
    listChuaGhiDanh: (payload: any) => api.post<User>('/LayDanhSachNguoiDungChuaGhiDanh', payload),
    getListUserCourses: (payload: any) => api.post<User>('/LayDanhSachHocVienKhoaHoc', payload),
    postListUserWaiting: (payload: any) => api.post<User>('/LayDanhSachHocVienChoXetDuyet', payload),
    
    getSearchUser: (keywords: string, isGroupCode: string) => api.get<User>(`/TimKiemNguoiDung?MaNhom=${isGroupCode}&tuKhoa=${keywords}`),
    deleteUser: (taiKhoan: string) => api.delete<User>(`/XoaNguoiDung?TaiKhoan=${taiKhoan}`),
    postListCourseUnregistered: (id: string) => api.post<User>(`/LayDanhSachKhoaHocChuaGhiDanh?TaiKhoan=${id}`),
    ListCourseRegistered: (data: string) => api.post<User>(`/LayDanhSachKhoaHocDaXetDuyet` ,data),
    postListCourseWaiting: (payload: any) => api.post<User>('/LayDanhSachKhoaHocChoXetDuyet', payload),
    putUpdateUser: (data: any) => api.put(`/CapNhatThongTinNguoiDung`, data),
    postAddUser: (data: any) => api.post<User>('/ThemNguoiDung', data),
    
    

    
    
}