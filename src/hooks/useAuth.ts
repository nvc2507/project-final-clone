import { useSelector } from 'react-redux'
import { RootState } from 'stores'

export const useAuth = () => { 
    const { accessToken, user, listUser } = useSelector((state: RootState) => state.quanLyNguoiDung);
    return { accessToken, user, listUser }
}