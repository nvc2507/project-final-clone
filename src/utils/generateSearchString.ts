/**
 * @param: object
 * @returns: search String đùng để query URL 
 */
// JS docs

import qs from 'qs'

export const generateSearchString = (params: {}) => { 
    const seachString = qs.stringify(params, {
        addQueryPrefix: true,
    })
    return seachString;
}