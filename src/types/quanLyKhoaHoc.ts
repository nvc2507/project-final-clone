export type DanhMucKhoaHoc = {
    maDanhMuc?: string
    tenDanhMuc?: string
    maDanhMucKhoahoc?: string
    tenDanhMucKhoaHoc?: string
}
export type NguoiTao = {
    taiKhoan?: string
    hoTen?: string
    maLoaiNguoiDung?: string
    tenLoaiNguoiDung?: string
}
export type KhoaHocTheoDanhMuc = {
    maKhoaHoc?: string
    biDanh?: string
    tenKhoaHoc?: string
    moTa?: string
    luotXem?: number
    hinhAnh?: string
    maNhom?: string
    ngayTao?: string
    soLuongHocVien?: number
    nguoiTao?: NguoiTao
    danhMucKhoaHoc?: DanhMucKhoaHoc

}
export type danhSachKhoaHoc = KhoaHocTheoDanhMuc[]

export type QueryLayKhoaHocTheoDanhMuc = {
    maDanhMuc?: string,
    maNhom?: string
}
export type DanhSachKhoaHoc_PhanTrang = {
    currentPage?: number
    count?: number
    totalPages?: number
    totalCount?: number
    items?: KhoaHocTheoDanhMuc[]
}
export type QueryLayDanhSachKhoaHoc_PhanTrang = {
    tenKhoaHoc?: string
    page?: number
    pageSize?: number
    maNhom?: string
}

export type ghiDanhKH = {
    maKhoaHoc?: string
    taiKhoan?: string
}

export type listHVKH = {
    maKhoaHoc?: string
    taiKhoan?: string
    biDanh?: string
}