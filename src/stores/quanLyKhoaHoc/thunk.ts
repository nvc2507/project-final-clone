import { createAsyncThunk } from '@reduxjs/toolkit'
import { quanLyKhoaHocServices } from 'services';
import {  QueryLayDanhSachKhoaHoc_PhanTrang, QueryLayKhoaHocTheoDanhMuc  } from 'types';

export const layDanhMucKhoaHocThunk = createAsyncThunk('quanLyKhoaHoc/layDanhMucKhoaHocThunk', async (_,{rejectWithValue}) => { 
    try {
        const data = await quanLyKhoaHocServices.layDanhMucKhoaHoc();
        return data.data;
    } catch (error) {
        return rejectWithValue(error);
    }
})

export const layKhoaHocTheoDanhMucThunk = createAsyncThunk('quanLyKhoaHoc/layKhoaHocTheoDanhMucThunk', async (payload: QueryLayKhoaHocTheoDanhMuc, {rejectWithValue}) => {
    try {
        const data = await quanLyKhoaHocServices.layKhoaHocTheoDanhMuc(payload);
        return data.data;
    } catch (error) {
        return rejectWithValue(error);
    }
})

export const layDanhSachKhoaHoc_PhanTrangThunk = createAsyncThunk('quanLyKhoaHoc/layDanhSachKhoaHoc_PhanTrangThunk', async (payload: QueryLayDanhSachKhoaHoc_PhanTrang, {rejectWithValue}) => {
    try {
        const data = await quanLyKhoaHocServices.layDanhSachKhoaHoc_PhanTrang(payload);
        return data.data;
    } catch (error) {
        return rejectWithValue(error);
    }
})

export const layThongTinKhoaHocThunk = createAsyncThunk('quanLyKhoaHoc/layThongTinKhoaHocThunk',async (payload: string, {rejectWithValue}) => {
    try {
        const data = await quanLyKhoaHocServices.layThongTinKhoaHoc(payload);
        return data.data;
    } catch (error) {
        return rejectWithValue(error);
    }
})
export const LayDanhSachKhoaHocthunk = createAsyncThunk('quanLyKhoaHoc/LayDanhSachKhoaHocthunk',async (payload: string, {rejectWithValue}) => {
    try {
        const data = await quanLyKhoaHocServices.getListCourseSearch(payload);
        return data.data;
    } catch (error) {
        return rejectWithValue(error);
    }
})

export const ghiDanhKhoaHocThunk = createAsyncThunk('QuanLyKhoaHoc/ghiDanhKhoaHocThunk', async (payload: any , {rejectWithValue}) => {
    try {
        const data = await quanLyKhoaHocServices.ghiDanhKhoaHoc(payload)
        return data.data;
    } catch (error) {
        if (error.response) {
            return rejectWithValue(error.response.data);
          }
          return rejectWithValue('Có lỗi xảy ra khi ghi danh khóa học.');
    }
})
export const deleteGhiDanhKhoaHocThunk = createAsyncThunk('QuanLyKhoaHoc/deleteGhiDanhKhoaHocThunk', async (payload: any , {rejectWithValue}) => {
    try {
        const data = await quanLyKhoaHocServices.deleteGhiDanh(payload)
        return data.data;
    } catch (error) {
        return rejectWithValue(error)
    }
})

export const deleteKhoaHocThunk = createAsyncThunk('QuanLyKhoaHoc/deleteKhoaHocThunk' , async (MaKhoaHoc: any , {rejectWithValue}) => {
    try {
        const data = await quanLyKhoaHocServices.xoaKhoaHoc(MaKhoaHoc)
        return data.data
    } catch (error) {
        return rejectWithValue(error)
    }
})

export const addKhoaHocThunk = createAsyncThunk('QuanLyKhoaHoc/addKhoaHocThunk' , async (payload: any , {rejectWithValue}) => {
    try {
        const data = await quanLyKhoaHocServices.themKhoaHoc(payload)
        return data.data
    } catch (error) {
        return rejectWithValue(error)
    }
})

export const getInfoUserThunk = createAsyncThunk('QuanLyKhoaHoc/getInfoUserThunk' , async (_, {rejectWithValue}) => {
    try {
        const data = await quanLyKhoaHocServices.getInfoUser()
        return data.data
    } catch (error) {
        return rejectWithValue(error)
    }
})

export const AdmlayDanhSachKhoaHocThunk = createAsyncThunk('QuanLyKhoaHoc/AdmlayDanhSachKhoaHocThunk' , async (payload: string, {rejectWithValue}) => {
    try {
        const data = await quanLyKhoaHocServices.layDanhSachKhoaHoc(payload)
        return data.data
    } catch (error) {
        return rejectWithValue(error)
    }
})
export const UploadHinhAnhKhoaHocThunk = createAsyncThunk('QuanLyKhoaHoc/UploadHinhAnhKhoaHocThunk' , async (formData: FormData , {rejectWithValue}) => {
    try {
        const data = await quanLyKhoaHocServices.uploadHinhAnh(formData)
        return data
    } catch (error) {
        return rejectWithValue(error)
    }
})