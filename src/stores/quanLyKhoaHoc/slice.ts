import { createSlice } from "@reduxjs/toolkit"
import { DanhMucKhoaHoc, DanhSachKhoaHoc_PhanTrang, KhoaHocTheoDanhMuc, danhSachKhoaHoc, ghiDanhKH } from 'types'
import { LayDanhSachKhoaHocthunk, addKhoaHocThunk, deleteGhiDanhKhoaHocThunk, deleteKhoaHocThunk, getInfoUserThunk, ghiDanhKhoaHocThunk, layDanhMucKhoaHocThunk, layDanhSachKhoaHoc_PhanTrangThunk, layKhoaHocTheoDanhMucThunk, layThongTinKhoaHocThunk } from "./thunk"

 type QuanLyKhoaHocState = {
    danhMucKhoaHoc?: DanhMucKhoaHoc[],
    khoaHocTheoDanhMuc?: KhoaHocTheoDanhMuc[],
    currentPage?: number,
    danhSachKhoaHoc?: DanhSachKhoaHoc_PhanTrang,
    thongTinKhoaHoc?: KhoaHocTheoDanhMuc,
    listCourses?: danhSachKhoaHoc,
    ghiDanh?: ghiDanhKH,
    isLoading: boolean
}

const initialState: QuanLyKhoaHocState = {
    danhMucKhoaHoc: [],
    khoaHocTheoDanhMuc: [],
    currentPage: 1,
    danhSachKhoaHoc: undefined,
    thongTinKhoaHoc: undefined,
    listCourses: [],
    ghiDanh: undefined ,
    isLoading: false
    
}

export const quanLyKhoaHocSlice = createSlice({
    name: 'quanLyKhoaHoc',
    initialState,
    reducers: {
        setCurrentPage: (state,action) => {
            state.currentPage = action.payload
        }
    },
    extraReducers: (build) => { 
        build
        .addCase(layDanhMucKhoaHocThunk.fulfilled, (state, {payload}) => { 
            state.danhMucKhoaHoc = payload;
        })
        .addCase(layKhoaHocTheoDanhMucThunk.fulfilled, (state, {payload}) => {
            state.khoaHocTheoDanhMuc = payload;
        })
        .addCase(layDanhSachKhoaHoc_PhanTrangThunk.fulfilled, (state, {payload}) => {
            state.danhSachKhoaHoc = payload;
        })
        .addCase(layThongTinKhoaHocThunk.fulfilled, (state, {payload}) => {
            state.thongTinKhoaHoc = payload;
        })
        .addCase(LayDanhSachKhoaHocthunk.fulfilled, (state, {payload}) => {
            state.listCourses = payload;
        })
        .addCase(ghiDanhKhoaHocThunk.fulfilled, (state, {payload}) => {
            state.ghiDanh = payload;
            state.isLoading = false
        })
        .addCase(ghiDanhKhoaHocThunk.rejected, (state) => {
           state.isLoading = false
        })
        .addCase(deleteGhiDanhKhoaHocThunk.fulfilled, (state, {payload}) => {
            state.ghiDanh = payload;
        })
        .addCase(deleteKhoaHocThunk.fulfilled, (state, {payload}) => {
            state.listCourses = payload
            
        })
        .addCase(addKhoaHocThunk.fulfilled, (state, {payload}) => {
            state.listCourses = payload
        })
        .addCase(getInfoUserThunk.fulfilled, (state, {payload}) => {
            state.ghiDanh = payload
        })
        
    }
})

export const { reducer: quanLyKhoaHocReducer, actions: quanLyKhoaHocActions } = quanLyKhoaHocSlice