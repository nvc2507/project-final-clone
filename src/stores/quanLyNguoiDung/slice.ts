import { createSlice } from '@reduxjs/toolkit'
import { deleteUserThunk, layDanhSachHVKhoaHocThunk, layDanhSachNguoiDungThunk, layDanhSachNguoiDung_PhanTrangThunk, layThongTinNguoiDungThunk, loginThunk, timKiemNguoiDungThunk } from './thunk'
import { DanhSachNguoiDung_PhanTrang, KhoaHocTheoDanhMuc, User } from 'types'

type QuanLyNguoiDungState = {
    user?: User,
    accessToken?: string,
    listUser?: DanhSachNguoiDung_PhanTrang,
    danhSachNguoiDung?: User[],
    danhSachNguoiDungTimKiem?: User[]

    danhSachKhoaHocChuaGhiDanh?: KhoaHocTheoDanhMuc[],
    danhSachKhoaHocChoXetDuyet?: KhoaHocTheoDanhMuc[],
    danhSachKhoaHocDaXetDuyet?: KhoaHocTheoDanhMuc[],
    currentPage?: number
}

const initialState: QuanLyNguoiDungState = {
    user: undefined,
    accessToken: undefined,
    listUser: undefined,
    danhSachNguoiDung: [],
    currentPage: 1,
    danhSachNguoiDungTimKiem: [],

    danhSachKhoaHocChuaGhiDanh: [],
    danhSachKhoaHocChoXetDuyet: [],
    danhSachKhoaHocDaXetDuyet: [],
    
}

export const quanLyNguoiDungSlice = createSlice({
    name: 'quanLyNguoiDung',
    initialState,
    reducers: {
        logOut: (state) => {
            state.user = undefined;
            state.accessToken = undefined;
            localStorage.removeItem('accessToken');
        },
        setCurrentPage: (state,action) => {
            state.currentPage = action.payload
        },
        setInfoUserDetail: (state, action) => {
            state.user = action.payload
        },
    },
    extraReducers: (build) => {
        build.addCase(loginThunk.fulfilled, (state,{payload}) => { 
            state.accessToken = payload.accessToken;
            localStorage.setItem('accessToken',JSON.stringify(state.accessToken));
        }).addCase(layThongTinNguoiDungThunk.fulfilled, (state, {payload}) => {
            state.user = payload;
        }).addCase(layDanhSachNguoiDung_PhanTrangThunk.fulfilled, (state, {payload}) => {
            state.listUser = payload;
        }).addCase(layDanhSachNguoiDungThunk.fulfilled, (state, {payload}) => {
            state.danhSachNguoiDung = payload;
        }).addCase(timKiemNguoiDungThunk.fulfilled, (state, {payload}) => {
            state.danhSachNguoiDungTimKiem = payload;
        })
        .addCase(layDanhSachHVKhoaHocThunk.fulfilled, (state, {payload}) => {
            state.user = payload;
        })
        .addCase(deleteUserThunk.fulfilled, (state, {payload}) => {
            state.user = payload;
        })
    }
})

export const { reducer: quanLyNguoiDungReducer, actions: quanLyNguoiDungActions } = quanLyNguoiDungSlice