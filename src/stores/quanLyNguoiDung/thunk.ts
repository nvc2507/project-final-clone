import { createAsyncThunk } from '@reduxjs/toolkit'
import { toast } from 'react-toastify';
import { AccountSchemaType, LoginSchemaType } from 'schema'
import { quanLyNguoiDungServices } from 'services'
import { QueryLayDanhSachNguoiDung_PhanTrang, QueryTimKiemNguoiDung } from 'types';

export const loginThunk = createAsyncThunk('quanLyNguoiDung/loginThunk',async (payload: LoginSchemaType, {rejectWithValue}) => {
    try {
        const data = await quanLyNguoiDungServices.login(payload);
        return data.data;
    } catch (error) {
        return rejectWithValue(error)
    }
});

export const layThongTinNguoiDungThunk = createAsyncThunk('quanLyNguoiDung/layThongTinNguoiDungThunk',async (payload, {rejectWithValue}) => {
    try {
        const data = await quanLyNguoiDungServices.layThongTinNguoiDung(payload);
        return data.data;
    } catch (error) {
        return rejectWithValue(error)
    }
});

export const layDanhSachNguoiDungThunk = createAsyncThunk('quanLyNguoiDung/layDanhSachNguoiDungThunk', async (_, {rejectWithValue}) => {
    try {
        const data = await quanLyNguoiDungServices.layDanhSachNguoiDung();
        return data.data;
    } catch (error) {
        return rejectWithValue(error);
    }
});

export const xoaNguoiDungThunk = createAsyncThunk('quanLyNguoiDung/xoaNguoiDungThunk',async (payload: string, {rejectWithValue}) => {
    try {
        await quanLyNguoiDungServices.xoaNguoiDung(payload);
        toast.success('Xóa thành công')
    } catch (error) {
        toast.error(error?.response?.data)
        return rejectWithValue(error);
    }
});

export const layDanhSachNguoiDung_PhanTrangThunk = createAsyncThunk('quanLyNguoiDung/layDanhSachNguoiDung_PhanTrangThunk', async (payload: QueryLayDanhSachNguoiDung_PhanTrang, {rejectWithValue}) => {
    try {
        const data = await quanLyNguoiDungServices.layDanhSachNguoiDung_PhanTrang(payload);
        return data.data;
    } catch (error) {
        return rejectWithValue(error)
    }
})


export const capNhatThongTinNguoiDungThunk = createAsyncThunk('quanLyNguoiDung/capNhatThongTinNguoiDungThunk',async (payload: AccountSchemaType, {rejectWithValue}) => {
    try {
        await quanLyNguoiDungServices.capNhatThongTinNguoiDung(payload);
        toast.success('Cập nhật thông tin thành công')
    } catch (error) {
        toast.error(error?.response?.data)
        return rejectWithValue(error);
    }
})

export const themNguoiDungThunk = createAsyncThunk('quanLyNguoiDung/themNguoiDungThunk',async (payload: AccountSchemaType, {rejectWithValue}) => {
    try {
        await quanLyNguoiDungServices.themNguoiDung(payload);
        toast.success('Thêm thành công');
    } catch (error) {
        toast.error(error.response.data);
        return rejectWithValue(error);
    }
})

export const timKiemNguoiDungThunk = createAsyncThunk('quanLyNguoiDung/timKiemNguoiDungThunk', async (payload: QueryTimKiemNguoiDung, {rejectWithValue}) => {
    try {
        const data = await quanLyNguoiDungServices.timKiemNguoiDung(payload);
        return data.data;
    } catch (error) {
        return rejectWithValue(error);
    }
})

export const listChuaGhiDanhThunk = createAsyncThunk('quanLyNguoiDung/listChuaGhiDanhThunk',async (payload, {rejectWithValue}) => {
    try {
        const data = await quanLyNguoiDungServices.listChuaGhiDanh(payload);
        return data.data;
    } catch (error) {
        return rejectWithValue(error)
    }
})
export const layDanhSachHVKhoaHocThunk = createAsyncThunk('quanLyNguoiDung/layDanhSachHVKhoaHocThunk',async (payload: any, {rejectWithValue}) => {
    try {
        const data = await quanLyNguoiDungServices.getListUserCourses(payload);
        return data.data;
    } catch (error) {
        return rejectWithValue(error)
    }
})
export const postListUserWaitingThunk = createAsyncThunk('quanLyNguoiDung/postListUserWaitingThunk',async (payload: any, {rejectWithValue}) => {
    try {
        const data = await quanLyNguoiDungServices.postListUserWaiting(payload);
        return data.data;
    } catch (error) {
        return rejectWithValue(error)
    }
})

export const getListUserThunk = createAsyncThunk('quanLyNguoiDung/getListUserThunk',async (payload: any, {rejectWithValue}) => {
    try {
        const data = await quanLyNguoiDungServices.getListUser(payload);
        return data.data;
    } catch (error) {
        return rejectWithValue(error)
    }
})
export const postListCourseWaitingThunk = createAsyncThunk('quanLyNguoiDung/postListCourseWaitingThunk',async (payload: any, {rejectWithValue}) => {
    try {
        const data = await quanLyNguoiDungServices.postListCourseWaiting(payload);
        return data.data;
    } catch (error) {
        return rejectWithValue(error)
    }
})
export const ListCourseRegisteredThunk = createAsyncThunk('quanLyNguoiDung/ListCourseRegisteredThunk',async (payload: any, {rejectWithValue}) => {
    try {
        const data = await quanLyNguoiDungServices.ListCourseRegistered(payload);
        return data.data;
    } catch (error) {
        return rejectWithValue(error)
    }
})
export const deleteUserThunk = createAsyncThunk('quanLyNguoiDung/deleteUserThunk',async (taiKhoan: any, {rejectWithValue}) => {
    try {
        const data = await quanLyNguoiDungServices.deleteUser(taiKhoan);
        return data.data;
    } catch (error) {
        return rejectWithValue(error)
    }
})
