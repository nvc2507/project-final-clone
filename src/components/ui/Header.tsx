import styled from "styled-components";
import { Avatar, Input, Popover } from ".";
import {
  ShoppingCartOutlined,
  GlobalOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { NavLink, useNavigate } from "react-router-dom";
import { PATH } from "constant";
import { useAuth } from "hooks";
import { FaAlignLeft } from "react-icons/fa";
import { useEffect, useState } from "react";
import { RootState, useAppDispatch } from "stores";
import {
  layDanhMucKhoaHocThunk,
  layKhoaHocTheoDanhMucThunk,
} from "stores/quanLyKhoaHoc/thunk";
import { useSelector } from "react-redux";
import { quanLyNguoiDungActions } from "stores/quanLyNguoiDung/slice";

export const Header = () => {
  const navigate = useNavigate();
  const { user } = useAuth();
  const dispatch = useAppDispatch();
  const [maDanhMuc, _] = useState("");
  const accessTokenJson = localStorage.getItem("accessToken");
  const { danhMucKhoaHoc } = useSelector(
    (state: RootState) => state.quanLyKhoaHoc
  );
  const [isMenuVisible, setMenuVisible] = useState(false);

  const toggleMenu = () => {
    setMenuVisible(!isMenuVisible);
  };

  useEffect(() => {
    dispatch(layDanhMucKhoaHocThunk());
  }, [dispatch]);

  useEffect(() => {
    if (maDanhMuc) {
      dispatch(
        layKhoaHocTheoDanhMucThunk({
          maDanhMuc: maDanhMuc,
          maNhom: user?.maNhom,
        })
      );
    }
  }, [dispatch, maDanhMuc]);

  const onSearch = (tenKhoaHoc: string) => {
    if (tenKhoaHoc) {
      navigate(`${PATH.searchPage.replace(":tenKhoaHoc", tenKhoaHoc)}`);
    }
  };

  const content = (
    <ul className="flex flex-col w-[180px] ">
      {danhMucKhoaHoc?.map((khoaHoc) => (
        <NavLink
          to={PATH.detailCourses.replace(":maDanhMuc", khoaHoc.maDanhMuc)}
          className="hover:cursor-pointer !text-black  hover:bg-zinc-400 hover:!text-white text-16"
          key={khoaHoc.maDanhMuc}
        >
          {khoaHoc.tenDanhMuc}
        </NavLink>
      ))}
    </ul>
  );

  return (
    <HeaderS>
      <nav className="bg-white border-gray-200 px-4 lg:flex items-center justify-between py-2.5 dark:bg-gray-800 space-x-20">
          <div className="md:block">
            <a href="/" >
              <img
                src="https://www.udemy.com/staticx/udemy/images/v7/logo-udemy.svg"
                alt="Udemy"
                className="mr-3 h-[60px] w-[60px] "
                loading="lazy"
              />
            </a>
          </div>
        <div className="flex flex-wrap justify-between items-center mx-auto max-w-screen-xl">
          <div className="  lg:hidden">
            <button
              onClick={toggleMenu}
              className="text-black hover:text-gray-300 focus:outline-none menu--toggle"
            >
              <svg
                className="h-32 w-32"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 6h16M4 12h16m-7 6h7"
                ></path>
              </svg>
            </button>
          </div>
          <div
            className={`lg:flex items-center toggle__menu ${
              isMenuVisible ? "block" : "hidden"
            } mt-4 md:mt-0`}
          >
            <div className=" px-3 py-2 block !items-center ">
              <Popover
                content={content}
                className="content flex items-center justify-center cursor-pointer"
              >
                <span className="pr-3 font-600 ">Danh Mục</span> <FaAlignLeft />
              </Popover>
            </div>
            <div className=" items-center px-3 py-2 block">
              <Input.Search
                className="inputSearch ml-2"
                placeholder="Tìm kiếm"
                onSearch={onSearch}
                style={{
                  maxWidth: 250,
                }}
              />
            </div>
            <div className="  px-3 py-2 block ">
              <Popover
                content={
                  <div className="flex flex-col justify-center items-center">
                    <p
                      className="text-center mb-10"
                      style={{
                        width: 300,
                      }}
                    >
                      Cho phép nhóm của bạn truy cập vào hơn 22.000 khóa học
                      hàng đầu của Udemy, ở mọi nơi và mọi lúc.
                    </p>
                    <button
                      style={{
                        width: 200,
                        height: 50,
                      }}
                      className="bg-black hover:bg-gray-600 transition-all duration-300 text-white font-bold py-2 px-4 rounded"
                    >
                      Dùng thử Udemy Business
                    </button>
                  </div>
                }
                className="content font-500 cursor-pointer"
              >
                Udemy Business
              </Popover>
            </div>
            <div className="  px-3 py-2 block ">
              <Popover
                className="content font-500 cursor-pointer"
                content={
                  <div>
                    <p>
                      Biến kiến thức của bạn thành cơ hội và tiếp cận với hàng
                      triệu người trên thế giới.
                    </p>
                    <button className="bg-black hover:bg-gray-600 transition-all duration-300 text-white font-bold py-2 px-4 rounded">
                      Dùng thử Udemy Business
                    </button>
                  </div>
                }
              >
                Teaching on Udemy
              </Popover>
            </div>
            <div className="   px-3 py-2 block ">
              {accessTokenJson && (
                <Popover
                  className="content font-500 cursor-pointer"
                  content={
                    <div>
                      <button className="bg-black hover:bg-gray-600 transition-all duration-300 text-white font-bold py-2 px-4 rounded">
                        Chuyển đến Quá trình học của tui
                      </button>
                    </div>
                  }
                >
                  Study Process
                </Popover>
              )}
            </div>
            <div className="  px-3 py-2 flex items-center justify-center ">
              <ShoppingCartOutlined className="text-26 font-600 cursor-pointer" />
            </div>
            <div className=" user--icon flex  items-center justify-center px-3 py-2 ">
              <div className="flex  ">
                {!accessTokenJson && (
                  <button
                    onClick={() => {
                      navigate(PATH.login);
                    }}
                    className="text-14 bg-white hover:bg-gray-400 text-black font-bold py-8 px-8 border border-black"
                  >
                    Đăng nhập
                  </button>
                )}
                {!accessTokenJson && (
                  <button
                    onClick={() => {
                      navigate(PATH.register);
                    }}
                    className="text-14 bg-black hover:bg-gray-600 text-white font-bold py-8 px-12 border border-black rounded"
                  >
                    Đăng ký
                  </button>
                )}
                {!accessTokenJson && (
                  <div className="border border-black flex items-center">
                    <GlobalOutlined className="p-10" />
                  </div>
                )}
                {accessTokenJson && (
                  <Popover
                    content={
                      <div className="p-2 ">
                        <h2 className="font-600 mb-4 p-2">{user?.hoTen}</h2>
                        <hr />

                        {user?.maLoaiNguoiDung === "GV" && (
                          <div
                            onClick={() => {
                              window.location.href = PATH.adminCourse;
                            }}
                            className="p-2 mt-4 hover:cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300"
                          >
                            Trang Admin
                          </div>
                        )}
                        <div
                          onClick={() => {
                            window.location.href = PATH.profile;
                          }}
                          className="p-2 mt-4 hover:cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300"
                        >
                          Thông tin tài khoản
                        </div>
                        <div
                          className="p-2 mt-4 hover:cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300"
                          onClick={() => {
                            dispatch(quanLyNguoiDungActions.logOut());
                            navigate(PATH.login);
                          }}
                        >
                          Đăng xuất
                        </div>
                      </div>
                    }
                    trigger={"click"}
                  >
                    <Avatar
                      className="!ml-12 hover:cursor-pointer !flex !items-center !justify-center"
                      size={40}
                      icon={<UserOutlined></UserOutlined>}
                    ></Avatar>
                  </Popover>
                )}
              </div>{" "}
            </div>
          </div>
        </div>

        
      </nav>
    </HeaderS>
  );
};

export default Header;

const HeaderS = styled.header`
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  background: white;
  z-index: 50;

  padding: 0 90px;
  height: var(--header-height);
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.08), 0 4px 12px rgba(0, 0, 0, 0.08);
  nav {
    max-width: 90%;
    margin: auto;
    height: 100%;
  }

  .menu--toggle {
    position: absolute;
    top: 25px;
    right: 25px;
  }

  @media screen and (max-width: 768px) {
    .toggle__menu {
    background-color: #b7b7b7;
    position: absolute;
    top: 70px;
    right: 0;
    width: 250px;

    .user--icon {
      margin: 0;
    }
  }
  }
  @media screen and (max-width: 1023px) {
    .toggle__menu {
      background-color: #b7b7b7;
    position: absolute;
    top: 70px;
    right: 0;
    width: 250px;
    }

    .user--icon {
      margin: 0;
    }
    
  }
`;
