import { HTMLInputTypeAttribute } from 'react'
import { UseFormRegister } from 'react-hook-form'

type InputFormProps = {
    type?: HTMLInputTypeAttribute,
    placeholder?: string,
    register?: UseFormRegister<any>,
    name?: string,
    errors?: string,
    disable?: boolean,
    label?: string
    onChange?: (e: any) => void;
}

export const InputForm = ({ disable, label, type, placeholder, onChange,  register, name, errors }: InputFormProps) => {
  return (
    <div className='py-10 '>
        {
          label && <p className="mb-2">{label}</p> 
        }
        <input disabled = {disable} type={type} onChange={onChange} className="h-[40px] border border-black rounded-xl text-black leading-10 pl-4 text-sm text-16 block w-full !p-10" placeholder={placeholder}
            {... register(name)}/>
        <p className='text-red-500'>{errors}</p>
    </div>
  )
}

export default InputForm