import { Table as TableA, TableProps as TablePropsA } from 'antd'

export const Table = (props: TablePropsA<any>) => {
  return (
    <TableA {... props}></TableA>
  )
}

export default Table