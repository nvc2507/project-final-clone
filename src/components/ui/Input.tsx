import { Input as InputA, InputProps as InputPropsA } from 'antd';
import { SearchProps } from 'antd/es/input';

type InputObject = {
    (props: InputPropsA): JSX.Element
    Search: React.FC<SearchProps>
};

export const Input: InputObject = ( props ) => {
  return (
    <InputA {... props}></InputA>
  )
};

Input.Search = InputA.Search;

export default Input;
