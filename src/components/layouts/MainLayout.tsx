import { Footer, Header } from "components/ui"
import styled from "styled-components"
import { Outlet } from 'react-router-dom'

export const MainLayout = () => {
  return (
    <div>
        <Header></Header>
        <Container >
            <Outlet></Outlet>
        </Container>
        <Footer></Footer>
    </div>
  )
}

export default MainLayout

const Container = styled.main`
    position:relative;
    /* max-width: var(--max-width); */
    max-width: 100vw;
`