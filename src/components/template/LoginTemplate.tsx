import { zodResolver } from "@hookform/resolvers/zod"
import { SubmitHandler, useForm } from "react-hook-form"
import { LoginSchema, LoginSchemaType } from "schema"
import { useNavigate } from 'react-router-dom'
import { PATH } from "constant"
import { loginThunk } from "stores/quanLyNguoiDung/thunk"
import { InputForm } from "components/ui"
import { useAppDispatch } from "stores"
import { toast } from 'react-toastify'

export const LoginTemplate = () => {
  const dispatch = useAppDispatch();

  const { handleSubmit, formState: {errors}, register } = useForm<LoginSchemaType>({
    mode: "onChange",
    resolver: zodResolver(LoginSchema)
  })

  const navigate = useNavigate();

  const onSubmit: SubmitHandler<LoginSchemaType> = async (value) => { 
    try {
      await dispatch(loginThunk(value)).unwrap().then(() => {
        toast.success('Đăng nhập thành công');
        navigate('/');
      });
    } catch (error) {
      toast.error(error?.response?.data);
    }
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <h2 className="text-black font-500 text-18">Đăng nhập vào tài khoản Udemy của bạn</h2>
      <div className="mt-10">
        <InputForm type="text" placeholder="Tài khoản" errors={errors?.taiKhoan?.message as string} register={register} name="taiKhoan" />
      </div>
      <div className="mt-10">
        <InputForm type="password" placeholder="Mật khẩu" errors={errors?.matKhau?.message as string} register={register} name="matKhau" />
      </div>
      <div className="mt-16 flex flex-col items-center">
            <button type="submit" className="w-full h-[60px] outline-none font-700 text-white bg-purple-600 hover:bg-purple-800 transition-all duration-200 text-16 px-5 py-2.5 mr-2 mb-2">Đăng nhập</button>
            <hr className="bg-gray-400 h-[0.1px] w-full mt-10" />
            <p className="text-black text-14 mt-10">Bạn không có tài khoản? <span className=" text-black text-14" >Hãy <a onClick={() => { navigate(PATH.register) }} className="underline font-700 text-purple-700 cursor-pointer">đăng ký</a></span></p>
        </div>
    </form>
  )
}

export default LoginTemplate