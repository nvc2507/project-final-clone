import { FieldTimeOutlined, GlobalOutlined } from "@ant-design/icons";
import ButtonV2 from "components/ui/ButtonV2/ButtonV2";
import { useEffect, useCallback  } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { RootState, useAppDispatch } from "stores";
import { layThongTinKhoaHocThunk , ghiDanhKhoaHocThunk} from "stores/quanLyKhoaHoc/thunk";

export const DetailTemplate = () => {
  const dispatch = useAppDispatch();
  const { maKhoaHoc} = useParams<{ maKhoaHoc: string }>();

  console.log(maKhoaHoc);
  

  const { thongTinKhoaHoc ,} = useSelector(
    (state: RootState) => state.quanLyKhoaHoc
  );
  const { user } = useSelector(
    (state: RootState) => state.quanLyNguoiDung
  );

  const handleGhiDanh = useCallback (() => {
    const data = {

      maKhoaHoc: thongTinKhoaHoc.maKhoaHoc,
      taiKhoan: user?.taiKhoan

    }

    dispatch(ghiDanhKhoaHocThunk(data))
    .unwrap()
    .then(()=> {
      toast.success('Bạn đã ghi danh thành công')
    })
    .catch((error) => {
      toast.error(error)
    })
  },  [dispatch, thongTinKhoaHoc, user])

  const fetchThongTinKhoaHoc = useCallback(() => {
    dispatch(layThongTinKhoaHocThunk(maKhoaHoc));
  }, [dispatch, maKhoaHoc]);

  

  useEffect(() => {
    fetchThongTinKhoaHoc()
  }, [fetchThongTinKhoaHoc]);


  

  const nguoiTao = thongTinKhoaHoc?.nguoiTao;
  const danhMucKhoaHoc = thongTinKhoaHoc?.danhMucKhoaHoc;



  return (
    <div className="Detail  ">
      <div className="content lg:container  items-center gap-x-[10px] !mx-[100px] py-[100px]  grid grid-cols-1 sm:grid-cols-2 justify-items-center ">
        <div>
          <img
          className="w-16 md:w-32 lg:w-48"
            src={thongTinKhoaHoc?.hinhAnh}
            style={{
              width: 350,
              // height: 400,
              objectFit: "cover",
            }}
            alt="..."
          />
        </div>
        <div className="flex flex-col gap-y-20 w-full">
          <p className="font-700 text-purple-600 text-18">
            {danhMucKhoaHoc?.maDanhMucKhoahoc} &#62;{" "}
            {danhMucKhoaHoc?.tenDanhMucKhoaHoc}
          </p>
          <h2 className="font-600 text-18">Khóa học: {thongTinKhoaHoc?.tenKhoaHoc}</h2>
          <p>Chi tiết: {thongTinKhoaHoc?.moTa}</p>
          <p>Số lượng học viên: {thongTinKhoaHoc?.soLuongHocVien}</p>
          <p>
            Được tạo bởi:{" "}
            <a className="font-600 text-purple-600 underline" href="">
              {nguoiTao?.hoTen}
            </a>
          </p>
          <div className="flex gap-x-10">
            <div>
              <FieldTimeOutlined /> Ngày tạo:{" "}
              <span>{thongTinKhoaHoc?.ngayTao}</span>
            </div>
            <div>
              <GlobalOutlined /> Việt Nam
            </div>
          </div>
          <div><button onClick={handleGhiDanh}>

            <ButtonV2
            >
              Đăng Ký
            </ButtonV2>
          </button>
          </div>
        </div>
      </div>
      
    </div>
  );
};

export default DetailTemplate;
