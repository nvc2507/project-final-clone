import { zodResolver } from "@hookform/resolvers/zod/src/zod.js"
import { SubmitHandler, useForm } from "react-hook-form"
import { RegisterSchema, RegisterSchemaType } from "schema"
import { useNavigate } from "react-router-dom";
import { PATH } from "constant";
import { toast } from 'react-toastify'
import { quanLyNguoiDungServices } from "services";
import { AxiosError } from "axios";
import { InputForm } from "components/ui";

export const RegisterTemplate = () => {
  const navigate = useNavigate();

  const { handleSubmit, formState: {errors}, register } = useForm<RegisterSchemaType>({
    mode: "onChange",
    resolver: zodResolver(RegisterSchema)
  })

  const onSubmit: SubmitHandler<RegisterSchemaType> = async (value) => { 
    try {
      await quanLyNguoiDungServices.register(value);
      toast.success('Đăng ký thành công');
      navigate(PATH.login)
    } catch (error: unknown) {
      if(error instanceof AxiosError){
        toast.error(error?.response?.data);
      }
    }
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <h2 className="text-black font-500 text-18">Đăng ký tài khoản Udemy của bạn</h2>
      <div className="mt-10">
        <InputForm type="text" placeholder="Tài khoản" errors={errors?.taiKhoan?.message as string} register={register} name="taiKhoan" />
      </div>
      <div className="mt-10">
        <InputForm type="password" placeholder="Mật khẩu" errors={errors?.matKhau?.message as string} register={register} name="matKhau" />
      </div>
      <div className="mt-10">
        <InputForm type="text" placeholder="Họ và tên" errors={errors?.hoTen?.message as string} register={register} name="hoTen"/>
      </div>
      <div className="mt-10">
        <InputForm type="text" placeholder="Số điện thoại" errors={errors?.soDT?.message as string} register={register} name="soDT" />
      </div>
      <div className="mt-10">
        <InputForm type="text" placeholder="Mã nhóm" errors={errors?.maNhom?.message as string} register={register} name="maNhom" />
      </div>
      <div className="mt-10">
        <InputForm type="text" placeholder="Email" errors={errors?.email?.message as string} register={register} name="email" />
      </div>
      <div className="mt-16 flex flex-col items-center">
            <button type="submit" className="w-full h-[60px] outline-none font-700 text-white bg-purple-600 hover:bg-purple-800 transition-all duration-200 text-16 px-5 py-2.5 mr-2 mb-2">Đăng ký</button>
            <hr className="bg-gray-400 h-[0.1px] w-full mt-10" />
            <p className="text-black text-14 mt-10">Bạn đã có tài khoản? <span className=" text-black text-14" >Hãy <a onClick={() => { navigate(PATH.login) }} className="underline font-700 text-purple-700 hover:cursor-pointer">đăng nhập</a></span></p>
        </div>
    </form>
  )
}

export default RegisterTemplate