import { Badge, Typography } from "antd";
import { Card, Pagination } from "components/ui";
import Banner from "components/ui/Banner";
import InfoCourseHome from "components/ui/InfoCourseHome";
import Instrutor from "components/ui/Instrutor/Instrutor";
import NumberCountUp from "components/ui/NumberCountUp/NumberCountUp";
import { PATH } from "constant";
import { useAuth } from "hooks";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { generatePath, useNavigate } from "react-router-dom";
import { RootState, useAppDispatch } from "stores";
import { quanLyKhoaHocActions } from "stores/quanLyKhoaHoc/slice";
import { layDanhSachKhoaHoc_PhanTrangThunk } from "stores/quanLyKhoaHoc/thunk";

export const HomeTemplate = () => {
  const dispatch = useAppDispatch();

  const handlePageChange = (page: number) => {
    dispatch(quanLyKhoaHocActions.setCurrentPage(page));
  };
  const { user } = useAuth();
  const { currentPage, danhSachKhoaHoc } = useSelector(
    (state: RootState) => state.quanLyKhoaHoc
  );

  const navigate = useNavigate();

  useEffect(() => {
    dispatch(
      layDanhSachKhoaHoc_PhanTrangThunk({
        page: currentPage,
        pageSize: 10,
        maNhom: user?.maNhom,
      })
    );
  }, [dispatch, currentPage, user]);

  return (
    <div className="HomeTemplate overflow-hidden">
      <Banner />
      <div className="lg:container mx-auto"> <InfoCourseHome/> </div>
      <h2 className="uppercase text-center text-transparent mt-20 bg-clip-text text-3xl pb-10 bg-gradient-to-r to-zinc-800 from-zinc-600">
        Danh sách khóa học
      </h2>
      <div className="w-fit grid grid-cols-1 lg:grid-cols-4 md:grid-cols-2 justify-items-center justify-center gap-y-20 gap-x-14    mb-5 lg:container mx-auto">
        {danhSachKhoaHoc?.items?.map((khoaHoc) => {
          return (
            <div key={khoaHoc?.maKhoaHoc} className="w-68 bg-zinc-200 shadow-md rounded-xl duration-500 hover:scale-105 hover:shadow-xl">
              
              <Badge.Ribbon text="Được yêu thích" color="Red">
                <Card
                  title={<Typography.Text>{khoaHoc.danhMucKhoaHoc.tenDanhMucKhoaHoc}</Typography.Text>}
                  key={khoaHoc?.maKhoaHoc}
                  hoverable
                  style={{ width: 288, margin: "0 auto" }}
                  cover={
                    <img
                      style={{ height: 250 }}
                      alt="..."
                      src={khoaHoc?.hinhAnh}
                    />
                  }
                  onClick={() => {
                    navigate(
                      generatePath(PATH.detail, {
                        maKhoaHoc: khoaHoc.maKhoaHoc,
                      })
                    );
                  }}
                >
                 <div > <Card.Meta
                    title={khoaHoc?.tenKhoaHoc}
                    description={khoaHoc?.moTa?.substring(0, 100)}
                
                  /></div>
                </Card>
              </Badge.Ribbon>
            </div>
          );
        })}
      </div>
      <Pagination
        className="text-center !my-20"
        defaultCurrent={1}
        onChange={handlePageChange}
        total={57}
      ></Pagination>

      <NumberCountUp />
        <div className="lg:container mx-auto"> <Instrutor/> </div>

    </div>
  );
};

export default HomeTemplate;
