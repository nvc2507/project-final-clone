import { InputForm } from 'components/ui'
import { useAuth } from 'hooks'
import { useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { styled } from 'styled-components'


export const AccountInfoTab = () => {
    const { user } = useAuth();

    const { reset, register } = useForm()

    useEffect(() => {
        reset(user)
    }, [user, reset])

    return (
        <div>
            <h2 className=' uppercase text-center text-zinc-500 font-bold text-4xl'>Thông tin Tài khoản</h2>
            <form className="px-40 ">
                <InputS label="Tài khoản" name="taiKhoan" register={register} />
                <InputS label="Họ và tên" name="hoTen" register={register} />
                <InputS label="Số điện thoại" name="soDT" register={register} />
                <InputS label="Email" name="email" register={register} />
                <InputS label="Mã nhóm" name="maNhom" register={register} />
                <InputS disable label="Loại người dùng" name="maLoaiNguoiDung" register={register} />
            </form>
        </div>
    )
}

export default AccountInfoTab

const InputS = styled(InputForm)`
    margin-top: 20px;
    input {
        background-color: transparent !important;
        border: 1px solid black;
        color: black;
    }
`
