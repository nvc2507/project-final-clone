import { RootState, useAppDispatch } from "stores";
import { useEffect, useState } from "react"
import { capNhatThongTinNguoiDungThunk, layDanhSachNguoiDungThunk,  themNguoiDungThunk, timKiemNguoiDungThunk, xoaNguoiDungThunk } from "stores/quanLyNguoiDung/thunk";
import { Button, Input, InputForm, Modal, Table } from "components/ui";
import styled from "styled-components";
import { useSelector } from "react-redux";
import { ColumnsType } from "antd/es/table";
import { User } from "types";
import { SubmitHandler, useForm } from "react-hook-form";
import { AccountSchema, AccountSchemaType } from "schema";
import { zodResolver } from "@hookform/resolvers/zod";
import { useAuth } from "hooks";

export const AccountListStudentTab = () => {
  const { user } = useAuth();
  const { danhSachNguoiDung, danhSachNguoiDungTimKiem } = useSelector((state: RootState) => state.quanLyNguoiDung);
  
   const dispatch = useAppDispatch();
   useEffect(() => { dispatch(layDanhSachNguoiDungThunk()) }, [dispatch]);

   const { register, reset, handleSubmit, formState: {errors} } = useForm<AccountSchemaType>({
    mode: 'onChange',
    resolver: zodResolver(AccountSchema)
   });
   const [userFocus, setUserFocus] = useState(undefined);
   useEffect(() => { reset(userFocus) },[userFocus]);

   // Modal Update
   const [isModalOpen1, setIsModalOpen1] = useState(false);
   const showModal1 = (user: User) => { 
        setIsModalOpen1(true);
        setUserFocus(user);
    };
   const handleCancel1 = () => { setIsModalOpen1(false) };
   const onSubmit1: SubmitHandler<AccountSchemaType> = (value) => {
    dispatch(capNhatThongTinNguoiDungThunk(value)).unwrap().then(() => {
        dispatch(layDanhSachNguoiDungThunk()); 
        setIsModalOpen1(false);
    });
   };

   // Modal Create
   const [isModalOpen2, setIsModalOpen2] = useState(false);
   const showModal2 = () => { setIsModalOpen2(true); };
   const onSubmit: SubmitHandler<AccountSchemaType> = (value) => { dispatch(themNguoiDungThunk(value)).unwrap().then(() => { dispatch(layDanhSachNguoiDungThunk());
     setIsModalOpen2(false);     
    }) };
   const handleCancel2 = () => { setIsModalOpen2(false) };

   // Table 
    const columns: ColumnsType<User> = [
        {
          title: 'Tài khoản',
          dataIndex: 'taiKhoan',
          key: 'taiKhoan',        },
        {
          title: 'Họ và tên',
          dataIndex: 'hoTen',
          key: 'hoTen',
          responsive: ['lg']
        },
        {
          title: 'Số điện thoại',
          dataIndex: 'soDT',
          key: 'soDT',
          responsive: ['lg']
        },
        {
            title: 'Mã nhóm',
            dataIndex: 'maNhom',
            key: 'maNhom',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
            responsive: ['lg']
        },
        {
            title: 'Mã loại người dùng',
            dataIndex: 'maLoaiNguoiDung',
            key: 'maLoaiNguoiDung',
        },
        {
            title: 'Tên loại người dùng',
            dataIndex: 'tenLoaiNguoiDung',
            key: 'tenLoaiNguoiDung',
            responsive: ['lg']
        },
        {
            title: 'Action',
            key: 'action',
            render: (_, user) => (
                <div>
                    <Button onClick={() => { 
                        dispatch(xoaNguoiDungThunk(user.taiKhoan)).unwrap().then(() => { 
                            dispatch(layDanhSachNguoiDungThunk())
                        })
                     }} type="primary" danger>
                        Xóa
                    </Button>
                    <Button onClick={() => { showModal1(user) }} className="!bg-yellow-400 !text-black !ml-10" type="primary" >
                        Cập nhật
                    </Button>
                </div>
            ),
            responsive: ['lg']     
        }
    ];
    const data: User[] = [];
    const dataTable = (list: User[]) => { 
        for (let index = 0; index < list?.length; index++) {
            const { taiKhoan,hoTen,soDt,maNhom,email,maLoaiNguoiDung } = list[index];
    
            data.push({
                key: index + 1,
                taiKhoan,
                hoTen,
                soDT: soDt,
                maNhom: !maNhom ? 'Không' : maNhom,
                email,
                maLoaiNguoiDung,
                tenLoaiNguoiDung: maLoaiNguoiDung == 'HV' ? 'Học viên' : 'Giáo viên'
            });
        };
    }
    dataTable(!danhSachNguoiDungTimKiem.length ? danhSachNguoiDung : danhSachNguoiDungTimKiem)
    
  return (
    <div>
        <h2 className=' uppercase text-center text-zinc-500 font-bold text-4xl mb-20'>Danh sách người dùng</h2>
        <div className="flex justify-between">
            <div className="hidden lg:flex lg:flex-1">
                <Input.Search
                    className="inputSearch ml-2"
                    placeholder="Tìm kiếm"
                    onSearch={(tuKhoa: string) => { 
                        dispatch(timKiemNguoiDungThunk({
                            tuKhoa: tuKhoa ? tuKhoa : undefined,
                            MaNhom: user?.maNhom
                        }))
                    }}
                    style={{
                        maxWidth: 400,
                    }}
                />
            </div>
            <Button onClick={showModal2} type="primary" className="mb-10 mr-20">Thêm người dùng</Button>
        </div>
        <Table columns={columns} dataSource={data}></Table>
        <Modal footer="" title="Thông tin người dùng" open={isModalOpen1}  onCancel={handleCancel1}>
        <form onSubmit={handleSubmit(onSubmit1)}>
                    <InputS label="Tài khoản" name="taiKhoan" register={register} errors={errors?.taiKhoan?.message}/>
                    <InputS disable type="password" label="Mật khẩu" name="matKhau" register={register} errors={errors?.matKhau?.message} />
                    <InputS label="Họ và tên" name="hoTen" register={register} type={errors?.hoTen?.message}/>
                    <InputS label="Số điện thoại" name="soDT" register={register} type={errors?.soDT?.message}/>
                    <InputS label="Mã nhóm" name="maNhom" register={register} type={errors?.maNhom?.message}/>
                    <InputS disable label="Loại người dùng" name="maLoaiNguoiDung" register={register} type={errors?.maLoaiNguoiDung?.message}/>
                    <InputS label="Email" name="email" register={register} type={errors?.email?.message}/>
                    <Button htmlType="submit" type="primary" className="!bg-yellow-500 !text-black mt-20">Cập nhật</Button>
            </form>
        </Modal>
        <Modal footer="" title="Đăng ký người dùng" open={isModalOpen2} onCancel={handleCancel2}>
            <form onSubmit={handleSubmit(onSubmit)}>
                    <InputS label="Tài khoản" name="taiKhoan" register={register} errors={errors?.taiKhoan?.message}/>
                    <InputS type="password" label="Mật khẩu" name="matKhau" register={register} errors={errors?.matKhau?.message} />
                    <InputS label="Họ và tên" name="hoTen" register={register} type={errors?.hoTen?.message}/>
                    <InputS label="Số điện thoại" name="soDT" register={register} type={errors?.soDT?.message}/>
                    <InputS label="Mã nhóm" name="maNhom" register={register} type={errors?.maNhom?.message}/>
                    <InputS label="Loại người dùng" name="maLoaiNguoiDung" register={register} type={errors?.maLoaiNguoiDung?.message}/>
                    <InputS label="Email" name="email" register={register} type={errors?.email?.message}/>
                    <Button htmlType="submit" type="primary" className="!bg-green-500 !text-black mt-20">Thêm người dùng</Button>
            </form>
        </Modal>
        
    </div>
  )
}

export default AccountListStudentTab

const InputS = styled(InputForm)`
    margin-top: 20px;
    input {
        background-color: transparent !important;
        border: 1px solid black;
        color: black;
    }
`