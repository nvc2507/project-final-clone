import FilterSearch from './FilterSearch/FilterSearch'
import ResultSearch from './ResultSearch/ResultSearch'
import styles from './searchPageTemplate.module.scss'
// import  { setSpinnerOn, setSpinnerOff } from 'stores/spinnerSlice'
import TitlePage from 'components/TitlePage/TitlePage'
const SearchPageTemplate = () => {
  return (
    <div className={styles.searchPage}>
        <TitlePage title='TÌM KIẾM' text='KẾT QUẢ TÌM KIẾM KHÓA HỌC!!!'/>
            <div className={styles.searchContainer}>
                <div className={styles.item}>
                    <FilterSearch/>
                </div>
                <div className={styles.item}>
                    <ResultSearch />
                </div>
            </div>
    </div>
  )
}

export default SearchPageTemplate
